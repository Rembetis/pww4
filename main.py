import pygame
import numpy as np

canvas_width, canvas_height = 500, 500
screen = pygame.display.set_mode((canvas_width, canvas_height))

viewport_width, viewport_height = 4, 4
viewport_distance = 1

def viewport_to_canvas(x, y):
    return (x * canvas_width / viewport_width, y * canvas_height / viewport_height)

def project_vertex(v):
    x, y, z = v[0], v[1], v[2]
    if z == 0:
        z += 1
    return viewport_to_canvas(x * viewport_distance / z, y * viewport_distance / z)

def interpolate(i0, d0, i1, d1, p):
    if i0 == i1:
        return d0
    return int(d0 + (p - i0) * (d1 - d0) / (i1 - i0))

def put_pixel(x, y, color):
    x, y = int(x + (canvas_width / 2)), int(-y + (canvas_height / 2))
    screen.set_at((x, y), color)

def draw_line(v0, v1, color):
    #x0, y0, x1, y1 = int(v0[0] + (canvas_height / 2)), int(v0[1] + (canvas_height / 2)), int(v1[0] + (canvas_height / 2)), int(v1[1] + (canvas_height / 2))
    x0, y0, x1, y1 = int(v0[0]), int(v0[1]), int(v1[0]), int(v1[1])
    #pygame.draw.line(screen, color, (x0, y0), (x1, y1))

    #just go over it twice because im too lazy to do it properly
    for x in range(min(x0, x1), max(x0, x1)):
        y = interpolate(x0, y0, x1, y1, x)
        put_pixel(x, y, color)
    
    for y in range(min(y0, y1), max(y0, y1)):
        x = interpolate(y0, x0, y1, x1, y)
        put_pixel(x, y, color)

def draw_triangle(x0, y0, x1, y1, x2, y2, color):
    #sort points
    if y1 < y0:
        (x1, y1), (x0, y0) = (x0, y0), (x1, y1)
    if y2 < y0:
        (x2, y2), (x0, y0) = (x0, y0), (x2, y2)
    if y2 < y1:
        (x2, y2), (x1, y1) = (x1, y1), (x2, y2)

    #find side coordinates
    side0, side1, side2 = [], [], []
    for y in range(y0, y1):
        side0.append(interpolate(y0, x0, y1, x1, y))
    for y in range(y1, y2):
        side1.append(interpolate(y1, x1, y2, x2, y))
    for y in range(y0, y2):
        side2.append(interpolate(y0, x0, y2, x2, y))

    side0 = side0[:-1]
    side_comb = side0 + side1

    #left and right
    m = int(np.floor(len(side_comb) / 2))

    if side2[m] < side_comb[m]:
        x_left = side2
        x_right = side_comb
    else:
        x_left = side_comb
        x_right = side2

    #fill
    for y in range(y0, y2 - 1):
        for x in range(x_left[y - y0], x_right[y - y0]):
            put_pixel(x, y, color)

def draw_wire_cube(position, size, color):
    xpos, ypos, zpos = position[0], position[1], position[2]

    #front
    vAf = [-size + xpos, size + ypos, size + zpos]
    vBf = [size + xpos, size + ypos, size + zpos]
    vCf = [size + xpos, -size + ypos, size + zpos]
    vDf = [-size + xpos, -size + ypos, size + zpos]

    #back
    vAb = [-size + xpos, size + ypos, size * 2 + zpos]
    vBb = [size + xpos, size + ypos, size * 2 + zpos]
    vCb = [size + xpos, -size + ypos, size * 2 + zpos]
    vDb = [-size + xpos, -size + ypos, size * 2 + zpos]

    draw_line(project_vertex(vAf), project_vertex(vBf), "blue")
    draw_line(project_vertex(vBf), project_vertex(vCf), "blue")
    draw_line(project_vertex(vCf), project_vertex(vDf), "blue")
    draw_line(project_vertex(vDf), project_vertex(vAf), "blue")

    draw_line(project_vertex(vAb), project_vertex(vBb), "red")
    draw_line(project_vertex(vBb), project_vertex(vCb), "red")
    draw_line(project_vertex(vCb), project_vertex(vDb), "red")
    draw_line(project_vertex(vDb), project_vertex(vAb), "red")

    draw_line(project_vertex(vAf), project_vertex(vAb), "green")
    draw_line(project_vertex(vBf), project_vertex(vBb), "green")
    draw_line(project_vertex(vCf), project_vertex(vCb), "green")
    draw_line(project_vertex(vDf), project_vertex(vDb), "green")

draw_triangle(0, 0, 76, 84, 122, 0, "orange")
draw_wire_cube((-1, 0, 0.5), 0.2, "blue")
draw_wire_cube((0, 0, 0.5), 0.5, "blue")

#window
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_q:
                running = False

    pygame.display.flip()
pygame.quit()
quit()